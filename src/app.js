import cluster from 'cluster'
import OS from 'os'

const numCPUs = OS.cpus().length

if (cluster.isMaster) {
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork()
  }
} else {
  require('./index')
}
