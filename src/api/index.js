import { version } from '../../package.json'
import { Router } from 'express'
import controllers from './controllers'

export default ({ config, db }) => {
  let api = Router()

  // perhaps expose some API metadata at the root
  api.get('/', (req, res) => {
    res.json({ version })
  })

  api.get('/assetallocation', (req, res, next) => {
    controllers.transactionHistory(req.query, db)
      .then(results => res.json(results))
      .catch(err => next({
        message: 'Something went wrong',
        status: 500,
        error: err
      }))
  })

  api.get('/assetrecord', (req, res, next) => {
    controllers.assetRecord(req.query, db)
      .then(results => res.json(results))
      .catch(err => next({
        message: 'Something went wrong',
        status: 500,
        error: err
      }))
  })

  api.get('/assetrecord/info', (req, res, next) => {
    controllers.assetRecordInfo(req.query, db)
      .then(results => res.json(results))
      .catch(err => next({
        message: 'Something went wrong',
        status: 500,
        error: err
      }))
  })

  return api
}
