import transactionHistory from './transaction-history'
import assetRecord from './asset-record'
import assetRecordInfo from './asset-record-info'

export default {
  assetRecord,
  assetRecordInfo,
  transactionHistory
}
