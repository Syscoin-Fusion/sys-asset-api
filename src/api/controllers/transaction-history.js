export default (query, models) => {
  return new Promise((resolve, reject) => {
    let { alias, asset, symbol } = query

    const dbQuery = {
      $or: [{
        sender: alias
      }, {
        receiver: alias
      }]
    }

    if (asset) {
      dbQuery.asset = asset
    }
    if (symbol) {
      dbQuery.symbol = symbol
    }

    models.AssetAllocation.find(dbQuery, {
      __v: 0, _id: 0
    }).then(results => resolve(results)).catch(err => reject(err))
  })
}
