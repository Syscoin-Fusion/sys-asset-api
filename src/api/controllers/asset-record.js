export default (query, models) => {
  return new Promise((resolve, reject) => {
    let { alias } = query

    const dbQuery = {
      alias
    }

    models.AssetRecord.find(dbQuery, {
      __v: 0, _id: 0
    }).then(results => resolve(results)).catch(err => reject(err))
  })
}
