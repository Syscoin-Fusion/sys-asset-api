export default (query, models) => {
    return new Promise((resolve, reject) => {
      let { asset } = query
  
      const dbQuery = {
        asset_id: asset
      }
  
      models.AssetRecord.find(dbQuery, {
        __v: 0, _id: 0
      }).then(results => resolve(results)).catch(err => reject(err))
    })
  }
  