import mongoose from 'mongoose'
import models from './models'

export default callback => {
  const DB_URI = process.env.MONGO_URL + ':' + process.env.MONGO_PORT
  mongoose.connect(DB_URI)

  callback(models)
}
