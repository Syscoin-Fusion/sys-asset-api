const mongoose = require('mongoose')

const AssetRecordSchema = new mongoose.Schema({
  asset_id: String,
  guid: {
    type: String,
    index: true
  },
  symbol: String,
  category: String,
  alias: {
    type: String,
    index: true
  },
  height: Number,
  use_input_ranges: Boolean,
  precision: Number,
  balance: Number,
  total_supply: Number,
  max_supply: Number,
  interest_rate: Number
})

export default mongoose.model('assetrecord', AssetRecordSchema)
