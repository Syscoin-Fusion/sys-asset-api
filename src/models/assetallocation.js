const mongoose = require('mongoose')

const AssetRecordSchema = new mongoose.Schema({
  txid: String,
  time: Number,
  asset: String,
  symbol: String,
  interest_rate: Number,
  height: Number,
  sender: {
    type: String,
    index: true
  },
  sender_balance: String,
  receiver: {
    type: String,
    index: true
  },
  receiver_balance: String,
  memo: String,
  confirmed: Boolean,
  amount: String
})

export default mongoose.model('assetallocation', AssetRecordSchema)
