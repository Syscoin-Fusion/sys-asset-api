import AssetRecord from './assetrecord'
import AssetAllocation from './assetallocation'

export default {
  AssetRecord,
  AssetAllocation
}
