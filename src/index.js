/* eslint import/first: 0 */
require('dotenv').config()
import http from 'http'
import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import initializeDb from './db'
import middleware from './middleware'
import api from './api'
import config from './config.json'

let app = express()
app.server = http.createServer(app)

// logger
app.use(morgan('dev'))

// 3rd party middleware
app.use(cors({
  exposedHeaders: config.corsHeaders
}))

app.use(bodyParser.json({
  limit: config.bodyLimit
}))

// connect to db
initializeDb(db => {
  // internal middleware
  app.use(middleware({ config, db }))

  // api router
  app.use('/api', api({ config, db }))

  // Error handling
  app.use((err, req, res, next) => {
    if (process.env.DEV_MODE) {
      console.log('Request query/body', req.query, req.body)
      console.log('Error: ', err)
    }

    if (err.message) {
      return res.status(err.status).json({
        message: err.message,
        status: err.status
      })
    } else {
      return res.status(500).json({
        message: 'Internal Server Error',
        status: 500
      })
    }
  })

  app.server.listen(process.env.PORT || config.port, () => {
    console.log(`Started on port ${app.server.address().port}`)
  })
})

export default app
